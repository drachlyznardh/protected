#!/usr/bin/python3
# -*- encoding: utf-8 -*-

import pkg_resources

from .aThing import AThing
from .anotherThing import AnotherThing

def main(args):
    with open(pkg_resources.resource_filename(__name__, 'VERSION'), 'r') as ifd:
        version = ifd.read().strip()
    print('Protected v{:s}'.format(version))
    AThing().do()
    AnotherThing().do()

