# Protected

This project is a minimal example of a working Python package having all its
meaningful sources compiled as they were C extensions using Cython, thus
protecting the source should you want to distribute a package as binary-only.
Protection is not absolute, but it's better against decompilation than using
.pyc files is.

## Install

Building Protected for your platform requires Cython. Run `pip install Cython`
and check whether a C compiler is available on your system, you may have to
install it separately.

### On Linux

Download the source and run `make` or `make linux` to build a wheel file, then
run `pip install <project dir>/dist/protected-<version>-<platform>.whl`, done.

### On Windows

Run `make windows` if you have Make installed or open a console and invoke
`py setup.py bdist_wininst`, then run the installer under the dist/ folder,
done.

