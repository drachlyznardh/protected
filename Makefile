
all: linux

linux:
	@python3 setup.py bdist_wheel

windows:
	@python3 setup.py bdist_wininst

clean:
	@$(RM) -r dist/ build/

veryclean:
	@find src/ -name '*.c' -delete
	@find src/ -name '*.egg-info' -type d | xargs $(RM) -rf
	@find src/ -name '__pycache__' -type d | xargs $(RM) -rf

.PHONY: all linux windows clean veryclean

