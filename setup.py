#!/usr/bin/python3
# -*- encoding: utf-8 -*-

from setuptools import setup, find_packages
from setuptools.extension import Extension
from distutils.command.build_py import build_py

from Cython.Build import cythonize
from Cython.Distutils import build_ext

with open('VERSION', 'r') as ofd:
    version = ofd.read().strip()

class MyBuildPy(build_py):

    def addCustomVersion(self):
        import sys, platform, os.path
        from distutils.file_util import copy_file
        _, srcdir, destdir, _ = self.data_files[0]
        isLinux = 'linux' in sys.platform    # Either Windows or Linux
        is64bit = '64' in platform.machine() # Either x86_64, amd64 or win32?
        src = os.path.join(srcdir, 'VERSION.{:s}.{:s}'.format('64' if is64bit else '32', 'linux' if isLinux else 'windows'))
        dest = os.path.join(destdir, 'VERSION')
        copy_file(src, dest)

    def run(self):
        build_py.run(self)
        if not self.dry_run: self.addCustomVersion()

setup(
    name = 'protected',
    version = version,
    ext_modules = cythonize(
        [ Extension('protected.*', ['src/protected/*.pyx']) ],
        compiler_directives = { 'always_allow_keywords': True }
    ),
    package_dir = { '': 'src' },
    packages = find_packages(where='src'),
    cmdclass = { 'build_py': MyBuildPy }
)

